<?php

namespace App\Model;

enum StarshipStatusEnum: string 
{
    case WAITING = 'watiing';
    case IN_PROGRESS = 'in progress';
    case COMPLETED = 'completed';
}