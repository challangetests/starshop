<?php

namespace App\Repository;

use App\Model\Starship;
use App\Model\StarshipStatusEnum;
use Psr\Log\LoggerInterface;

class StarshipRepository 
{
    public function __construct(private LoggerInterface $loggerInterface)
    {        
    }

    public function findAll(){
        $this->loggerInterface->info('starship retrieved');
        return [
            new Starship(
                1,
                "Glowworm",
                "cruiser",
                "Shikikan",
                StarshipStatusEnum::IN_PROGRESS,
            ),
            new Starship(
                2,
                "New Jersey",
                "battleship",
                "Shikikan",
                StarshipStatusEnum::WAITING,
            ),
            new Starship(
                3,
                "Musashi",
                "missle ship",
                "Shikikan",
                StarshipStatusEnum::COMPLETED,
            ),                         
           
        ];
    }

    public function find(int $id): ?Starship
    {
        foreach ($this->findAll() as $ship) {
            if ($ship->getId() ==  $id)
                return $ship;
        }

        return null;
    }
}