<?php

namespace App\Controller;

use App\Model\Starship;
use App\Repository\StarshipRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/starships')]
class StarshipApiController extends AbstractController
{
    public function __construct(
        private StarshipRepository $starshipRepository
    ) {        
    }

    #[Route('/', methods: ['GET'])]
    public function index(): Response
    {        
        $ships = $this->starshipRepository->findAll();

        return $this->json($ships);
    }

    #[Route('/{id<\d+>}', methods: ['GET'])]
    public function show(int $id): Response
    {        
        $ship = $this->starshipRepository->find($id);

        if (!$ship) {
            throw $this->createNotFoundException('Starship not found');
        }

        return $this->json($ship);
    }
}
