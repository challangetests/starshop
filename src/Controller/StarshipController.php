<?php

namespace App\Controller;

use App\Repository\StarshipRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/starships')]
class StarshipController extends AbstractController
{
    public function __construct(
        private StarshipRepository $starshipRepository
    ) {        
    }

    #[Route('/{id<\d+>}', methods: ['GET'], name: 'app_starship_show')]
    public function show(int $id): Response
    {        
        $ship = $this->starshipRepository->find($id);

        if (!$ship) {
            throw $this->createNotFoundException('Starship not found');
        }

        return $this->render('starship/show.html.twig',[
            'ship' => $ship
        ]);
    }
}
